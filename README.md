# r7ar - Alpha 2
Rapid snapshot archiver is a program forked from GNU Tar to change from tape archiver to a snapshot archiver using the incremental backup.

<h2>Prerequisites</h2>

- automake
- autoconf
- base-devel
- bison
- gcc
- git
- m4

The required packages may differ on some Linux distributions. 

<h2>Partial Changelog</h2>

- Replace the file if mtime age is different with disabled verbose output
- Removed legacy STAR archive header
- Removed configure autoconf version check*
- Removed incremental archiving if the ctime is different
